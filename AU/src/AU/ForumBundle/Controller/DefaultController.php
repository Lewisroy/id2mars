<?php

namespace AU\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AUForumBundle:Default:index.html.twig');
    }
}
