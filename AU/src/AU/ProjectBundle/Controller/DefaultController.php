<?php

namespace AU\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AUProjectBundle:Default:index.html.twig');
    }
}
