<?php

namespace AU\AdminBundle\Controller;

use AU\AdminBundle\Entity\Themes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Theme controller.
 *
 * @Route("themes")
 */
class ThemesController extends Controller
{
    /**
     * Lists all theme entities.
     *
     * @Route("/", name="themes_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $themes = $em->getRepository('AUAdminBundle:Themes')->findAll();

        return $this->render('themes/index.html.twig', array(
            'themes' => $themes,
        ));
    }

    /**
     * Creates a new theme entity.
     *
     * @Route("/new", name="themes_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $theme = new Theme();
        $form = $this->createForm('AU\AdminBundle\Form\ThemesType', $theme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($theme);
            $em->flush($theme);

            return $this->redirectToRoute('themes_show', array('id' => $theme->getId()));
        }

        return $this->render('themes/new.html.twig', array(
            'theme' => $theme,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a theme entity.
     *
     * @Route("/{id}", name="themes_show")
     * @Method("GET")
     */
    public function showAction(Themes $theme)
    {
        $deleteForm = $this->createDeleteForm($theme);

        return $this->render('themes/show.html.twig', array(
            'theme' => $theme,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing theme entity.
     *
     * @Route("/{id}/edit", name="themes_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Themes $theme)
    {
        $deleteForm = $this->createDeleteForm($theme);
        $editForm = $this->createForm('AU\AdminBundle\Form\ThemesType', $theme);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('themes_edit', array('id' => $theme->getId()));
        }

        return $this->render('themes/edit.html.twig', array(
            'theme' => $theme,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a theme entity.
     *
     * @Route("/{id}", name="themes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Themes $theme)
    {
        $form = $this->createDeleteForm($theme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($theme);
            $em->flush($theme);
        }

        return $this->redirectToRoute('themes_index');
    }

    /**
     * Creates a form to delete a theme entity.
     *
     * @param Themes $theme The theme entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Themes $theme)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('themes_delete', array('id' => $theme->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
