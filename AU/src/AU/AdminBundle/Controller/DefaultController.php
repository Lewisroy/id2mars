<?php

namespace AU\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AUAdminBundle:Default:index.html.twig');
    }
}
