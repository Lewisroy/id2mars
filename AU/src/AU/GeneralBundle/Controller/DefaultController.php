<?php

namespace AU\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AUGeneralBundle:Default:index.html.twig');
    }
}
