<?php

namespace AU\GeneralBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

class GeneralController
{
    public function indexAction()
    {
        return new Response("Notre propre Hello World !");
    }
}