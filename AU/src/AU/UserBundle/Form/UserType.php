<?php

namespace AU\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('birthday', 'datetime')
            ->add('gender')
            ->add('language')
            ->add('location')
            ->add('styles')
            ->add('tools')
            ->add('equipments')
            ->add('website')
            ->add('bio')
            ->add('socialmedia')
            ->add('profileType')
            ->add('defyMe')
            ->add('picture')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AU\UserBundle\Entity\User'
        ));
    }
}
