<?php

namespace AU\UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AU\UserBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", nullable = true)
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", nullable = true)
     */
    protected $lastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable = true)
     */
    protected $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable = true)
     */
    protected $gender;


    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable = true)
     */
    protected $language;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable = true)
     */
    protected $location;

    /**
     * @var text
     *
     * @ORM\Column(name="styles", type="text", nullable = true)
     */
    protected $styles;

    /**
     * @var string
     *
     * @ORM\Column(name="tools", type="string", nullable = true)
     */
    protected $tools;

    /**
     * @var string
     *
     * @ORM\Column(name="equipments", type="string", nullable = true)
     */
    protected $equipments;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", nullable = true)
     */
    protected $website;

    /**
     * @var text
     *
     * @ORM\Column(name="bio", type="text", nullable = true)
     */
    protected $bio;

    /**
     * @var string
     *
     * @ORM\Column(name="socialmedia", type="string", nullable = true)
     */
    protected $socialmedia;

    /**
     * @var boolean
     *
     * @ORM\Column(name="profile_type", type="boolean", nullable = true)
     */
    protected $profileType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="defy_me", type="boolean", nullable = true)
     */
    protected $defyMe;

    /**
     * @Assert\File(maxSize="2048k")
     * @Assert\Image(mimeTypesMessage="Please upload a valid image.")
     */
    private $file;

     /**
     * @ORM\Column(type="string", length=255, nullable=true) 
     */
    public $picture;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
 

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return User
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set styles
     *
     * @param $styles
     *
     * @return User
     */
    public function setStyles( $styles)
    {
        $this->styles = $styles;

        return $this;
    }

    /**
     * Get styles
     *
     * @return 
     */
    public function getStyles()
    {
        return $this->styles;
    }

    /**
     * Set tools
     *
     * @param string $tools
     *
     * @return User
     */
    public function setTools($tools)
    {
        $this->tools = $tools;

        return $this;
    }

    /**
     * Get tools
     *
     * @return string
     */
    public function getTools()
    {
        return $this->tools;
    }

    /**
     * Set equipments
     *
     * @param string $equipments
     *
     * @return User
     */
    public function setEquipments($equipments)
    {
        $this->equipments = $equipments;

        return $this;
    }

    /**
     * Get equipments
     *
     * @return string
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return User
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set bio
     *
     * @param string $bio
     *
     * @return User
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set socialmedia
     *
     * @param string $socialmedia
     *
     * @return User
     */
    public function setSocialmedia($socialmedia)
    {
        $this->socialmedia = $socialmedia;

        return $this;
    }

    /**
     * Get socialmedia
     *
     * @return string
     */
    public function getSocialmedia()
    {
        return $this->socialmedia;
    }

    /**
     * Set profileType
     *
     * @param boolean $profileType
     *
     * @return User
     */
    public function setProfileType($profileType)
    {
        $this->profileType = $profileType;

        return $this;
    }

    /**
     * Get profileType
     *
     * @return boolean
     */
    public function getProfileType()
    {
        return $this->profileType;
    }

    /**
     * Set defyMe
     *
     * @param boolean $defyMe
     *
     * @return User
     */
    public function setDefyMe($defyMe)
    {
        $this->defyMe = $defyMe;

        return $this;
    }

    /**
     * Get defyMe
     *
     * @return boolean
     */
    public function getDefyMe()
    {
        return $this->defyMe;
    }

      /**
 * @param UploadedFile $file
 * @return object
 */
public function setFile(UploadedFile $file = null)
{
    // set the value of the holder
    $this->file = $file;
    // check if we have an old image path
    if (isset($this->path)) {
        // store the old name to delete after the update
        $this->t = $this->profilePicturePath;
        $this->tempPath = null;
    } else {
        $this->path = 'initial';
    }

    return $this;
}

/**
 * Get the file used for profile picture uploads
 * 
 * @return UploadedFile
 */
public function getFile()
{

    return $this->file;
}

/**
 * @ORM\PrePersist() 
 * @ORM\PreUpdate() 
 */
public function preUpload()
{
    if (null !== $this->getFile()) {
        // a file was uploaded
        // generate a unique filename
        $filename = $this->generateRandomProfilePictureFilename();
        $this->setPath($filename);
    }
}

/**
 * Generates a 32 char long random filename
 * 
 * @return string
 */
public function generateRandomProfilePictureFilename()
{

        $random = md5(uniqid()).'.'.$this->getFile()->guessExtension();
    return $random;
}

/**
 * @ORM\PostPersist() 
 * @ORM\PostUpdate() 
 */
public function upload()
{
    if (null === $this->file) {
        return;
    }
    $this->getFile()->move($this->getUploadRootDir(), $this->getPath());

    if (isset($this->tempPath) && file_exists($this->getUploadRootDir() . '/' . $this->tempPath)) {
        unlink($this->getUploadRootDir() . '/' . $this->tempPath);
        $this->tempPath = null;
    }
    $this->file = null;
}

/**
 * @ORM\PostRemove() 
 */
public function removeUpload()
{
    if ($file == $this->getAbsolutePath()) {
        unlink($file);
    }
}

public function getAbsolutePath()
{
    return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
}

public function getWebPath()
{
    return null === $this->path ? null : $this->getUploadDir() . '/' .
            $this->id . '/' . $this->path;
}

protected function getUploadRootDir()
{
    return __DIR__ . '/../../../../web/' . $this->getUploadDir() . '/' . $this->id;
}

protected function getUploadDir()
{
    return 'uploads/users';
}

   /**
 * Set path
 *
 * @param string $path
 * @return User
 */
public function setPath($path)
{
    $this->picture = $path;

    return $this;
}
/**
 * Get path
 *
 * @return string 
 */
public function getPath()
{
    return $this->picture;
}
}
