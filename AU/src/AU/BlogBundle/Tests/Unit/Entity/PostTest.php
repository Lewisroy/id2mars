<?php
namespace AU\BlogBundle\Tests\Unit\Entity;

use AU\BlogBundle\Entity\Post;

class PostTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Post
     */
    private $objectToTest;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     * @see \DateTime Original class.
     */
    private $datetimeMock;

    public function setUp()
    {
        $this->datetimeMock = $this->getMock('\DateTime');

        $this->objectToTest = new Post($this->datetimeMock);
    }

    public function testConstructor()
    {
        $this->assertAttributeSame(
            $this->datetimeMock,
            'publicationDate',
            $this->objectToTest
        );
    }

    public function testGettersAndSetters()
    {
        $userMock = $this->getMock('AU\AppBundle\Entity\SystemUser');
        $dateTime = $this->getMock('\DateTime');
        $commentsMock = $this->getMock('Doctrine\Common\Collections\ArrayCollection');

        $properties = [
            'id' => 123,
            'author' => $userMock,
            'title' => 'sample title',
            'content' => 'sample content',
            'publicationDate' => $dateTime,
            'lastUpdateDate' => $dateTime,
            'comments' => $commentsMock
        ];

        foreach ($properties as $property=>$valueToUse) {
            $setter = sprintf('set%s', ucfirst($property));
            $getter = sprintf('get%s', ucfirst($property));

            $this->objectToTest->$setter($valueToUse);
            $this->assertAttributeSame($valueToUse, $property, $this->objectToTest);

            $this->assertSame($valueToUse, $this->objectToTest->$getter());
        }
    }
}
