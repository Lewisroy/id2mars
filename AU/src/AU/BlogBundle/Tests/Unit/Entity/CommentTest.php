<?php
namespace AU\BlogBundle\Tests\Unit\Entity;
use AU\BlogBundle\Entity\Comment;

/**
 * Comment
 * 
 * @package AU\BlogBundle\Tests\Entity
 */
class CommentTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Comment
     */
    private $objectToTest;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     * @see \DateTime Original class.
     */
    private $datetimeMock;

    public function setUp()
    {
        $this->datetimeMock = $this->getMock('\DateTime');
        $this->objectToTest = new Comment($this->datetimeMock);
    }

    public function testConstructor()
    {
        $this->assertAttributeSame(
            $this->datetimeMock,
            'creationDate',
            $this->objectToTest,
            'DateTime object was not injected correctly.'
        );
    }

    public function testGettersAndSetters()
    {
        $userMock = $this->getMock('AU\AppBundle\Entity\SystemUser');
        $post = $this->getMock('AU\BlogBundle\Entity\Post');
        $dateTime = $this->getMock('\DateTime');

        $properties = [
            'id' => 123,
            'author' => $userMock,
            'relatedPost' => $post,
            'content' => 'sample content',
            'creationDate' => $dateTime,
            'ip' => '4.5.6.7'
        ];

        foreach ($properties as $property=>$valueToUse) {
            $setter = sprintf('set%s', ucfirst($property));
            $getter = sprintf('get%s', ucfirst($property));

            $this->objectToTest->$setter($valueToUse);
            $this->assertAttributeSame($valueToUse, $property, $this->objectToTest);

            $this->assertSame($valueToUse, $this->objectToTest->$getter());
        }
    }
}
