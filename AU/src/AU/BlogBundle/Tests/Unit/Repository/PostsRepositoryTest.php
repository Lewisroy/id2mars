<?php
namespace AU\BlogBundle\Tests\Unit\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use AU\BlogBundle\Repository\PostsRepository;

/**
 * Class PostsRepositoryTest PHPUnit unit test case
 * @package AU\BlogBundle\Tests\Unit\Repository
 * @see PostsRepository Tested class.
 */
class PostsRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PostsRepository
     */
    private $objectToTest;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     * @see EntityManager Original class.
     */
    private $emMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     * @see ClassMetadata Original class.
     */
    private $mappingMock;

    public function setUp()
    {
        $this->emMock = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mappingMock = $this->getMockBuilder('Doctrine\ORM\Mapping\ClassMetadata')
            ->disableOriginalConstructor()
            ->getMock();

        $this->objectToTest = new PostsRepository(
            $this->emMock,
            $this->mappingMock
        );
    }

    public function testCreate()
    {
        $result = $this->objectToTest->create();
        $this->assertInstanceOf(
            'AU\BlogBundle\Entity\Post',
            $result
        );
    }
}
