<?php
namespace AU\BlogBundle\Tests\Unit\Controller;
use AU\BlogBundle\Controller\AdminController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * class AdminControllerTest PHPUnit unit test case
 * @see AdminController Original class.
 */
class AdminControllerTest extends \PHPUnit_Framework_TestCase
{
    const POST_ENTITY_CLASS = 'AU\BlogBundle\Entity\Post';
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     * @see ContainerInterface Original class.
     */
    private $containerMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     * @see Response Original class.
     */
    private $responseMock;

    /**
     * @var AdminController tested object
     */
    private $objectToTest;

    public function setUp()
    {
        $this->containerMock = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->getMock();

        $this->responseMock = $this->getMock('Symfony\Component\HttpFoundation\Response');

        $this->objectToTest = new AdminController(
            $this->containerMock
        );
    }

    public function testConstructor()
    {
        $this->containerMock = $this->getMock('Symfony\Component\DependencyInjection\ContainerInterface');
        $sampleContainerItem = 'item';

        $this->containerMock
            ->expects($this->exactly(8))
            ->method('get')
            ->withConsecutive(
                ['doctrine.orm.default_entity_manager'],
                ['templating'],
                ['response'],
                ['form.factory'],
                ['request_stack'],
                ['redirect_response'],
                ['router'],
                ['current_timestamp']
            )
            ->willReturn($sampleContainerItem);
        $objToTest = new AdminController($this->containerMock);

        $this->assertAttributeSame(
            $sampleContainerItem,
            'now',
            $objToTest
        );

        $this->assertAttributeSame(
            $this->containerMock,
            'container',
            $objToTest,
            'Service container object was not injected correctly.'
        );
    }

    public function testIndexAction()
    {
        $sampleContent = 'some content';
        $engineMock = $this->getMock('Symfony\Component\Templating\EngineInterface');

        $this->containerMock->expects($this->once())
            ->method('get')
            ->with('templating')
            ->willReturn($engineMock);

        $this->containerMock
            ->expects($this->once())
            ->method('get')
            ->willReturn($this->responseMock);

        $this->containerMock->expects($this->once())
            ->method('get')
            ->with('templating')
            ->willReturn($engineMock);

        $engineMock
            ->expects($this->once())
            ->method('render')
            ->with('BlogBundle:Admin:dashboard.html.twig')
            ->willReturn($sampleContent);

        $this->responseMock
            ->expects($this->once())
            ->method('setContent')
            ->with($sampleContent)
            ->willReturnSelf();

        $this->assertSame(
            $this->responseMock,
            $this->objectToTest->indexAction()
        );
    }

    public function testGetPostsAction()
    {
        $sampleContent = 'some content';

        $this->engineMock
            ->expects($this->once())
            ->method('render')
            ->with('BlogBundle:Admin:posts.html.twig')
            ->willReturn($sampleContent);

        $this->responseMock
            ->expects($this->once())
            ->method('setContent')
            ->with($sampleContent)
            ->willReturnSelf();

        $this->assertSame(
            $this->responseMock,
            $this->objectToTest->getPostsAction()
        );
    }

    public function testSavePostAction()
    {
        $this->responseMock
            ->expects($this->once())
            ->method('setContent')
            ->with('saving post')
            ->willReturnSelf();

        $this->assertSame(
            $this->responseMock,
            $this->objectToTest->savePostAction()
        );
    }

    public function testGetPostFormAction()
    {
        $sampleContent = 'some template content';

        $postMock = $this->getMockBuilder(self::POST_ENTITY_CLASS)
            ->getMock();

        $formMock = $this->getMockBuilder('AU\BlogBundle\Form\PostForm')
            ->setMethods(['createView'])
            ->getMock();

        $formViewMock = $this->getMock('Symfony\Component\Form\FormView');

        $repositoryMock = $this->getMockBuilder('AU\BlogBundle\Repository\PostsRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->emMock
            ->expects($this->once())
            ->method('getRepository')
            ->with('BlogBundle:Post')
            ->willReturn($repositoryMock);

        $repositoryMock->expects($this->once())
            ->method('create')
            ->willReturn($postMock);

        $this->formFactoryMock
            ->expects($this->once())
            ->method('create')
            ->with('AU\BlogBundle\Form\PostForm', $postMock)
            ->willReturn($formMock);

        $formMock->expects($this->once())
            ->method('createView')
            ->willReturn($formViewMock);

        $this->engineMock
            ->expects($this->once())
            ->method('render')
            ->with('BlogBundle:Admin:postForm.html.twig', ['form' => $formViewMock])
            ->willReturn($sampleContent);

        $this->responseMock
            ->expects($this->once())
            ->method('setContent')
            ->with($sampleContent)
            ->willReturnSelf();

        $this->assertSame(
            $this->responseMock,
            $this->objectToTest->getPostFormAction()
        );
    }
}
