<?php
namespace AU\BlogBundle\Tests\Unit\Form;
use AU\BlogBundle\Form\PostForm;

/**
 * class PostFormTest PHPUnit unit test case
 * @see PostForm Original class
 */
class PostFormTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PostForm tested object
     */
    private $objectToTest;

    public function setUp()
    {
        $this->objectToTest = new PostForm();
    }

    public function testBuildForm()
    {
        $builderMock = $this->getMockBuilder('Symfony\Component\Form\FormBuilderInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $builderMock
            ->expects($this->exactly(4))
            ->method('add')
            ->withConsecutive(
                ['publication_date', 'Symfony\Component\Form\Extension\Core\Type\DateTimeType'],
                ['title', 'Symfony\Component\Form\Extension\Core\Type\TextType'],
                ['content', 'Symfony\Component\Form\Extension\Core\Type\TextareaType'],
                ['save', 'Symfony\Component\Form\Extension\Core\Type\SubmitType']
            )
            ->willReturnSelf();

        $this->objectToTest->buildForm($builderMock, []);
    }
}
