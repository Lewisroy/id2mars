<?php
namespace AU\BlogBundle\Controller;
use Doctrine\ORM\EntityManagerInterface;
use AU\BlogBundle\BundleConfiguration;
use AU\BlogBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
/**
 * Class FrontController
 * @package AU\BlogBundle\Controller
 * @Route(service="kiss_plus_blog.controller.front_controller")
 */
class FrontController
{
    /**
     * @var TwigEngine
     */
    private $engine;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var BundleConfiguration
     */
    private $configuration;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var RedirectResponse
     */
    private $redirectResponse;
    /**
     * @param TwigEngine $engine
     * @param RouterInterface $router
     * @param EntityManagerInterface $entityManager
     * @param BundleConfiguration $configuration
     * @param RequestStack $requestStack
     * @param RedirectResponse $redirectResponse
     */
    public function __construct(
        TwigEngine $engine,
        RouterInterface $router,
        EntityManagerInterface $entityManager,
        BundleConfiguration $configuration,
        RequestStack $requestStack,
        RedirectResponse $redirectResponse
    ) {
        $this->engine = $engine;
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->configuration = $configuration;
        $this->requestStack = $requestStack;
        $this->redirectResponse = $redirectResponse;
    }
    /**
     * @return Response
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->engine->renderResponse(
            'BlogBundle:Front:index.html.twig',
            [
                'posts' => $this->entityManager
                    ->getRepository(Post::class)
                    ->findPageOfPublicated(1, $this->configuration->getPostsListPageSize())
            ]
        );
    }
    /**
     * @param string $slug
     * @Route(path="/{slug}")
     * @return Response
     */
    public function singlePostAction($slug)
    {
        $post = $this->entityManager
            ->getRepository('BlogBundle:Post')
            ->findOneBy(['uri' => $slug]);
        return $this->engine->renderResponse('BlogBundle:Front:singlePost.html.twig', ['post' => $post]);
    }
}