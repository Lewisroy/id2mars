<?php

namespace AU\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AUBlogBundle:Default:index.html.twig');
    }
}
