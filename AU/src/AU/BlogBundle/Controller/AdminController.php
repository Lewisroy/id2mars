<?php
namespace AU\BlogBundle\Controller;
use Doctrine\ORM\EntityManagerInterface;
use AU\BlogBundle\BundleConfiguration;
use AU\BlogBundle\Entity\Post;
use AU\BlogBundle\Handler\BlogPostFormHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
/**
 * @Route(service="kiss_plus_blog.controller.admin_controller")
 */
class AdminController
{
    /**
     * @var BlogPostFormHandler
     */
    private $formHandler;
    /**
     * @var TwigEngine
     */
    private $engine;
    /**
     * @var RedirectResponse
     */
    private $redirectResponse;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var BundleConfiguration
     */
    private $configuration;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * AdminController constructor.
     * @param BlogPostFormHandler $formHandler
     * @param TwigEngine $engine
     * @param RedirectResponse $redirectResponse
     * @param RouterInterface $router
     * @param EntityManagerInterface $entityManager
     * @param BundleConfiguration $configuration
     * @param RequestStack $requestStack
     */
    public function __construct(
        BlogPostFormHandler $formHandler,
        TwigEngine $engine,
        RedirectResponse $redirectResponse,
        RouterInterface $router,
        EntityManagerInterface $entityManager,
        BundleConfiguration $configuration,
        RequestStack $requestStack
    ) {
        $this->formHandler = $formHandler;
        $this->engine = $engine;
        $this->redirectResponse = $redirectResponse;
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->configuration = $configuration;
        $this->requestStack = $requestStack;
    }
    /**
     * @return Response
     * @Route("/", name="admin_blog_homepage")
     */
    public function indexAction()
    {
        return $this->engine->renderResponse('BlogBundle:Admin:dashboard.html.twig');
    }
    /**
     * @return Response
     * @Route("/posts/", methods={"GET"}, name="admin_blog_posts")
     */
    public function getPostsAction()
    {
        return $this->engine->renderResponse(
            'BlogBundle:Admin:posts.html.twig', [
                'posts' => $this->entityManager->getRepository(Post::class)
                    ->findPage(1, $this->configuration->getPostsListPageSize())
            ]
        );
    }
    /**
     * @return Response
     * @Route("/posts/add/", name="admin_blog_post_form")
     */
    public function postFormAction()
    {
        $formData = $this->formHandler->handle(0);
        if ($formData) {
            return $this->engine->renderResponse('BlogBundle:Admin:postForm.html.twig', ['form' => $formData]);
        }
        return $this->redirectResponse->setTargetUrl($this->router->generate('admin_blog_posts'));
    }
    /**
     * @param $id
     * @return RedirectResponse|Response
     * @Route("/posts/{id}/edit")
     */
    public function postEditFormAction($id)
    {
        $formData = $this->formHandler->handle($id);
        if ($formData) {
            return $this->engine->renderResponse('BlogBundle:Admin:postForm.html.twig', ['form' => $formData]);
        }
        return $this->redirectResponse->setTargetUrl($this->router->generate('admin_blog_posts'));
    }
    /**
     * @param int $id
     * @return RedirectResponse
     * @Route("/posts/{id}/delete")
     */
    public function deletePostAction($id)
    {
        $post = $this->entityManager->getRepository(Post::class)
            ->find($id);
        if($post) {
            $this->entityManager->remove($post);
            $this->entityManager->flush();
        }
        return $this->redirectResponse->setTargetUrl($this->router->generate('admin_blog_posts'));
    }
    /**
     * @param $id
     * @return RedirectResponse
     * @Route("/posts/{id}/publish-toggle")
     */
    public function togglePublicationAction($id)
    {
        $post = $this->entityManager->getRepository(Post::class)->find($id);
        if (!$post) {
            throwException('Post not found');
        }
        $post->setPublished(!$post->isPublished());
        $this->entityManager->persist($post);
        $this->entityManager->flush();
        return $this->redirectResponse->setTargetUrl($this->router->generate('admin_blog_posts'));
    }
}