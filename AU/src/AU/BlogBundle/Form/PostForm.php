<?php
namespace AU\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('publication_date', DateTimeType::class, ['label' => 'post.publication_date'])
            ->add('meta_description', TextareaType::class, ['label' => 'post.meta_description'])
            ->add('list_description', TextareaType::class, ['label' => 'post.list_description'])
            ->add('title', TextType::class, ['label' => 'post.title'])
            ->add('uri', TextType::class, ['label' => 'post.uri'])
            ->add(
                'content',
                TextareaType::class,
                ['label' => 'post.content', 'attr' => ['class' => 'tinyMce'], 'required' => false]
            )
            ->add('save', SubmitType::class, ['label' => 'post.save', 'attr' => ['class' => 'btn btn-success']]);
        if (!$options['data']->isPublished()){
            $builder->add('draft', SubmitType::class, [
                'label' => 'post.save_draft',
                'attr' => ['class' => 'btn btn-warning']
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['translation_domain' => 'admin', 'novalidate' => 'novalidate']);
    }
}