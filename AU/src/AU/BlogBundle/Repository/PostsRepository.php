<?php
namespace AU\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AU\BlogBundle\Entity\Post;
use Symfony\Component\Validator\Constraints\DateTime;

class PostsRepository extends EntityRepository
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @return Post
     */
    public function create()
    {
        return new Post(new \DateTime());
    }

    public function findPage($pageNumber, $pageSize)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.publicationDate', 'desc')
            ->setMaxResults($pageSize)
            ->setFirstResult(($pageNumber-1) * $pageSize)
            ->getQuery()
            ->execute();
    }

    public function findPageOfPublicated($pageNumber, $pageSize)
    {
        return $this->createQueryBuilder('p')
            ->where('p.publicationDate < :now')
            ->setParameter('now', (new \DateTime())->format(self::DATE_FORMAT))
            ->orderBy('p.publicationDate', 'desc')
            ->setMaxResults($pageSize)
            ->setFirstResult(($pageNumber-1) * $pageSize)
            ->getQuery()
            ->execute();
    }

    /**
     * @return int
     */
    public function getCountAll()
    {
        return (int)$this->getEntityManager()
            ->createQuery('SELECT COUNT(p.id) FROM BlogBundle:Post p')
            ->getSingleScalarResult();
    }

    public function getCountPublished()
    {
        return (int)$this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.publicationDate < :now')
            ->setParameter('now', (new \DateTime())->format(self::DATE_FORMAT))
            ->getQuery()
            ->getSingleScalarResult();
    }
}