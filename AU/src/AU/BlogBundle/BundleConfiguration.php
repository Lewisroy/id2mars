<?php
namespace AU\BlogBundle;

class BundleConfiguration
{
    /**
     * @var int
     */
    private $postsListPageSize;

    public function setConfig(array $config)
    {
        $this->postsListPageSize = $config['posts_list_page_size'];
    }

    /**
     * @return int
     */
    public function getPostsListPageSize()
    {
        return $this->postsListPageSize;
    }

    /**
     * @param int $postsListPageSize
     *
     * @return BundleConfiguration
     */
    public function setPostsListPageSize($postsListPageSize)
    {
        $this->postsListPageSize = $postsListPageSize;
        return $this;
    }


}