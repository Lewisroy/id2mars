<?php
namespace AU\BlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AU\AppBundle\Entity\SystemUser;

/**
 * @ORM\Entity(repositoryClass="AU\BlogBundle\Repository\PostsRepository")
 * @ORM\Table(name="blog_posts")
 */
class Post
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text", length=60, nullable=false)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $content;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $publicationDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $lastUpdateDate;

    /**
     * @var SystemUser
     * @ORM\ManyToOne(targetEntity="AU\AppBundle\Entity\SystemUser")
     * @ORM\JoinColumn(onDelete="SET NULL", referencedColumnName="id", name="system_user_id")
     */
    private $author;

    /**
     * @var ArrayCollection<Comment>
     * @ORM\OneToMany(targetEntity="AU\BlogBundle\Entity\Post", mappedBy="comments")
     */
    private $comments;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $published = false;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false, length=160)
     */
    private $metaDescription;

    /**
     * @var string Text do display as description on posts list
     * @ORM\Column(type="text", nullable=false, length=254)
     */
    private $listDescription;

    /**
     * @var string
     * @ORM\Column(type="string", length=254)
     */
    private $uri;

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return Post
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     * @return Post;
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getListDescription()
    {
        return $this->listDescription;
    }

    /**
     * @param string $listDescription
     * @return Post;
     */
    public function setListDescription($listDescription)
    {
        $this->listDescription = $listDescription;

        return $this;
    }

    public function __construct(\DateTime $publicationDate = null)
    {
        if(!$this->publicationDate) {
            $this->publicationDate = $publicationDate;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Post
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * @param \DateTime $publicationDate
     *
     * @return Post
     */
    public function setPublicationDate(\DateTime $publicationDate)
    {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdateDate()
    {
        return $this->lastUpdateDate;
    }

    /**
     * @param \DateTime $lastUpdateDate
     *
     * @return Post
     */
    public function setLastUpdateDate(\DateTime $lastUpdateDate)
    {
        $this->lastUpdateDate = $lastUpdateDate;
        return $this;
    }

    /**
     * @return SystemUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param SystemUser $author
     *
     * @return Post
     */
    public function setAuthor(SystemUser $author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param ArrayCollection $comments
     *
     * @return Post
     */
    public function setComments(ArrayCollection $comments)
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @param boolean $published
     * @return Post
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }


}