<?php
namespace AU\BlogBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use AU\AppBundle\Entity\SystemUser;

/**
 * @Entity
 * @Table(name="blog_comment")
 */
class Comment
{
    /**
     * @var integer
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var SystemUser
     * @ManyToOne(targetEntity="AU\AppBundle\Entity\SystemUser")
     * @JoinColumn(referencedColumnName="id", name="system_user_id")
     */
    private $author;

    /**
     * @var Post
     * @ManyToOne(targetEntity="AU\BlogBundle\Entity\Post", inversedBy="comments")
     * @JoinColumn(referencedColumnName="id", name="post_id")
     */
    private $relatedPost;

    /**
     * @var string
     * @Column(type="text")
     */
    private $content;

    /**
     * @var \DateTime
     * @Column(type="datetime")
     */
    private $creationDate;

    /**
     * @var string
     * @Column(type="text", length=45)
     */
    private $ip;

    public function __construct(\DateTime $creationDate = null)
    {
        if (!$this->creationDate) {
            $this->creationDate = $creationDate;
        }
    }

    /**
     * @return SystemUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param SystemUser $author
     */
    public function setAuthor(SystemUser $author)
    {
        $this->author = $author;
    }

    /**
     * @return Post
     */
    public function getRelatedPost()
    {
        return $this->relatedPost;
    }

    /**
     * @param Post $relatedPost
     */
    public function setRelatedPost(Post $relatedPost)
    {
        $this->relatedPost = $relatedPost;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}