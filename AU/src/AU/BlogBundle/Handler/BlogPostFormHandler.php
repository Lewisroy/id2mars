<?php
namespace AU\BlogBundle\Handler;

use Doctrine\ORM\EntityManagerInterface;
use AU\AppBundle\Entity\SystemUser;
use AU\AppBundle\Event\PostCreatedEvent;
use AU\AppBundle\Handler\BlogPostsStatistics;
use AU\BlogBundle\Entity\Post;
use AU\BlogBundle\Form\PostForm;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BlogPostFormHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var PostCreatedEvent
     */
    private $postCreatedEvent;

    /**
     * BlogPostFormHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param RequestStack $requestStack
     * @param TokenStorageInterface $tokenStorage
     * @param FormFactoryInterface $formFactory
     * @param EventDispatcherInterface $eventDispatcher
     * @param PostCreatedEvent $postCreatedEvent
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $requestStack,
        TokenStorageInterface $tokenStorage,
        FormFactoryInterface $formFactory,
        EventDispatcherInterface $eventDispatcher,
        PostCreatedEvent $postCreatedEvent
    ){
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->formFactory = $formFactory;
        $this->eventDispatcher = $eventDispatcher;
        $this->postCreatedEvent = $postCreatedEvent;
    }

    /**
     * @param int $id
     * @return null|\Symfony\Component\Form\FormView
     */
    public function handle($id)
    {
        $post = $this->createPost($id);
        $form = $this->formFactory
            ->create(PostForm::class, $post)
            ->handleRequest($this->requestStack->getCurrentRequest());

        if( $form->get('save')->isClicked() ) {
            $post->setPublished(true);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->savePost($post);
            $this->eventDispatcher->dispatch(PostCreatedEvent::POST_CREATED_EVENT, $this->postCreatedEvent);
            return null;
        }
        return $form->createView();
    }

    /**
     * @param Post $post
     */
    private function savePost(Post $post)
    {
        $post->setLastUpdateDate(new \DateTime());
        $this->entityManager->persist($post);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return Post
     */
    private function createPost($id)
    {
        $repo = $this->entityManager->getRepository('BlogBundle:Post');
        if (!$id) {
            return $repo->create()->setAuthor($this->tokenStorage->getToken()->getUser());
        }
        return $repo->find($id);
    }
}