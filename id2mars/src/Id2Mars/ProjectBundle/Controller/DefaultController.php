<?php

namespace Id2Mars\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('Id2MarsProjectBundle:Default:index.html.twig');
    }
}
