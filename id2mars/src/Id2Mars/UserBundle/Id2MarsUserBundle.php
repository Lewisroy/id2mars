<?php

namespace Id2Mars\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class Id2MarsUserBundle extends Bundle
{
	public function getParent()
  	{
    	return 'FOSUserBundle';
  	}
}
