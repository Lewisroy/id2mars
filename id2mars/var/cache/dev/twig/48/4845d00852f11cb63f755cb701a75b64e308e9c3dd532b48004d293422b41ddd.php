<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_7ce8d2bf73d3019b4f8971850f18f362e8fcabcd1ce45efef4a849c51d34c579 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6fb40bd445bd41370adbc8f2ade286588182c8ed8b48747c72e944a0f07efd87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fb40bd445bd41370adbc8f2ade286588182c8ed8b48747c72e944a0f07efd87->enter($__internal_6fb40bd445bd41370adbc8f2ade286588182c8ed8b48747c72e944a0f07efd87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_cf27c40f88ca600b365e6b97cf33f65bcc7b47408e23d10e147af79ccc50c07e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf27c40f88ca600b365e6b97cf33f65bcc7b47408e23d10e147af79ccc50c07e->enter($__internal_cf27c40f88ca600b365e6b97cf33f65bcc7b47408e23d10e147af79ccc50c07e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6fb40bd445bd41370adbc8f2ade286588182c8ed8b48747c72e944a0f07efd87->leave($__internal_6fb40bd445bd41370adbc8f2ade286588182c8ed8b48747c72e944a0f07efd87_prof);

        
        $__internal_cf27c40f88ca600b365e6b97cf33f65bcc7b47408e23d10e147af79ccc50c07e->leave($__internal_cf27c40f88ca600b365e6b97cf33f65bcc7b47408e23d10e147af79ccc50c07e_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_21292d55bedcafb475dd43ecc03c878e33d64201a340e986956de089fc3df9d5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21292d55bedcafb475dd43ecc03c878e33d64201a340e986956de089fc3df9d5->enter($__internal_21292d55bedcafb475dd43ecc03c878e33d64201a340e986956de089fc3df9d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_775ec915ca0c042f794387d8b37aa882acb6d5227a1d884f718ce3d4a610bfc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_775ec915ca0c042f794387d8b37aa882acb6d5227a1d884f718ce3d4a610bfc7->enter($__internal_775ec915ca0c042f794387d8b37aa882acb6d5227a1d884f718ce3d4a610bfc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_775ec915ca0c042f794387d8b37aa882acb6d5227a1d884f718ce3d4a610bfc7->leave($__internal_775ec915ca0c042f794387d8b37aa882acb6d5227a1d884f718ce3d4a610bfc7_prof);

        
        $__internal_21292d55bedcafb475dd43ecc03c878e33d64201a340e986956de089fc3df9d5->leave($__internal_21292d55bedcafb475dd43ecc03c878e33d64201a340e986956de089fc3df9d5_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_40a229c1c7e7877ead9eb0009d53bc08ce521636730da04d911a4ac6afa3c8f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40a229c1c7e7877ead9eb0009d53bc08ce521636730da04d911a4ac6afa3c8f2->enter($__internal_40a229c1c7e7877ead9eb0009d53bc08ce521636730da04d911a4ac6afa3c8f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e7419878d08bbee0c099f8d20e2a2713f51055f102efcaef83063e0c232c5ad4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7419878d08bbee0c099f8d20e2a2713f51055f102efcaef83063e0c232c5ad4->enter($__internal_e7419878d08bbee0c099f8d20e2a2713f51055f102efcaef83063e0c232c5ad4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_e7419878d08bbee0c099f8d20e2a2713f51055f102efcaef83063e0c232c5ad4->leave($__internal_e7419878d08bbee0c099f8d20e2a2713f51055f102efcaef83063e0c232c5ad4_prof);

        
        $__internal_40a229c1c7e7877ead9eb0009d53bc08ce521636730da04d911a4ac6afa3c8f2->leave($__internal_40a229c1c7e7877ead9eb0009d53bc08ce521636730da04d911a4ac6afa3c8f2_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5d72883e53ee3272ae561e8c05ec0cffd6e71f92a71c8df93447d4023c7bf316 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d72883e53ee3272ae561e8c05ec0cffd6e71f92a71c8df93447d4023c7bf316->enter($__internal_5d72883e53ee3272ae561e8c05ec0cffd6e71f92a71c8df93447d4023c7bf316_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_2322a94bca7c5d06b61b99c8b7fd99b95534cc7716c1f7b44f518b6a6572d7c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2322a94bca7c5d06b61b99c8b7fd99b95534cc7716c1f7b44f518b6a6572d7c5->enter($__internal_2322a94bca7c5d06b61b99c8b7fd99b95534cc7716c1f7b44f518b6a6572d7c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_2322a94bca7c5d06b61b99c8b7fd99b95534cc7716c1f7b44f518b6a6572d7c5->leave($__internal_2322a94bca7c5d06b61b99c8b7fd99b95534cc7716c1f7b44f518b6a6572d7c5_prof);

        
        $__internal_5d72883e53ee3272ae561e8c05ec0cffd6e71f92a71c8df93447d4023c7bf316->leave($__internal_5d72883e53ee3272ae561e8c05ec0cffd6e71f92a71c8df93447d4023c7bf316_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\Program Files (x86)\\EasyPHP-12.1\\www\\.git\\id2mars\\id2mars\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
