<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_3e4d1c38f476d8528f79c7a428f811cb73a92fa7274d2f7ac7f4b09a314b3635 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_92fdf559675cd33770f04f91ec447e3b67e715a3f006cc1ce16dc6265f266061 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92fdf559675cd33770f04f91ec447e3b67e715a3f006cc1ce16dc6265f266061->enter($__internal_92fdf559675cd33770f04f91ec447e3b67e715a3f006cc1ce16dc6265f266061_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_0e6bb46a60643e60ea80f052bba6f41056096bca6c25d9e1d7009ede1a9ba19e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e6bb46a60643e60ea80f052bba6f41056096bca6c25d9e1d7009ede1a9ba19e->enter($__internal_0e6bb46a60643e60ea80f052bba6f41056096bca6c25d9e1d7009ede1a9ba19e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_92fdf559675cd33770f04f91ec447e3b67e715a3f006cc1ce16dc6265f266061->leave($__internal_92fdf559675cd33770f04f91ec447e3b67e715a3f006cc1ce16dc6265f266061_prof);

        
        $__internal_0e6bb46a60643e60ea80f052bba6f41056096bca6c25d9e1d7009ede1a9ba19e->leave($__internal_0e6bb46a60643e60ea80f052bba6f41056096bca6c25d9e1d7009ede1a9ba19e_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "C:\\Program Files (x86)\\EasyPHP-12.1\\www\\.git\\id2mars\\id2mars\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\SecurityBundle\\Resources\\views\\Collector\\icon.svg");
    }
}
