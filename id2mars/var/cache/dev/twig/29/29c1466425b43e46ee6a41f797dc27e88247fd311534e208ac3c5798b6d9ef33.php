<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_639cd0f7355c23a757c16db065f1eda52cd93c5c9ded9d675cb289cf70eb8c69 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_79dc2abb70b5d31eda48a3819cb750bde1472511a634a9ec156a3c7c1864d685 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79dc2abb70b5d31eda48a3819cb750bde1472511a634a9ec156a3c7c1864d685->enter($__internal_79dc2abb70b5d31eda48a3819cb750bde1472511a634a9ec156a3c7c1864d685_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_a7ae9b1c839f20c34bb6128b7de7ef05d0da313d721bfbfe49c15122937f1c69 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7ae9b1c839f20c34bb6128b7de7ef05d0da313d721bfbfe49c15122937f1c69->enter($__internal_a7ae9b1c839f20c34bb6128b7de7ef05d0da313d721bfbfe49c15122937f1c69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_79dc2abb70b5d31eda48a3819cb750bde1472511a634a9ec156a3c7c1864d685->leave($__internal_79dc2abb70b5d31eda48a3819cb750bde1472511a634a9ec156a3c7c1864d685_prof);

        
        $__internal_a7ae9b1c839f20c34bb6128b7de7ef05d0da313d721bfbfe49c15122937f1c69->leave($__internal_a7ae9b1c839f20c34bb6128b7de7ef05d0da313d721bfbfe49c15122937f1c69_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c69f9faae6ef1d8daedc5c2f49d7ab785fa363cad1b30fd9437c15c485c43980 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c69f9faae6ef1d8daedc5c2f49d7ab785fa363cad1b30fd9437c15c485c43980->enter($__internal_c69f9faae6ef1d8daedc5c2f49d7ab785fa363cad1b30fd9437c15c485c43980_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_c43e781b6fbbfc36f92769b6e655326c11b393c7f020fe3909abd92eb7c083cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c43e781b6fbbfc36f92769b6e655326c11b393c7f020fe3909abd92eb7c083cf->enter($__internal_c43e781b6fbbfc36f92769b6e655326c11b393c7f020fe3909abd92eb7c083cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_c43e781b6fbbfc36f92769b6e655326c11b393c7f020fe3909abd92eb7c083cf->leave($__internal_c43e781b6fbbfc36f92769b6e655326c11b393c7f020fe3909abd92eb7c083cf_prof);

        
        $__internal_c69f9faae6ef1d8daedc5c2f49d7ab785fa363cad1b30fd9437c15c485c43980->leave($__internal_c69f9faae6ef1d8daedc5c2f49d7ab785fa363cad1b30fd9437c15c485c43980_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_25d7bec5c2511a63f7bc552894e730e616712ae4caa3003e1429103fc4007e2a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25d7bec5c2511a63f7bc552894e730e616712ae4caa3003e1429103fc4007e2a->enter($__internal_25d7bec5c2511a63f7bc552894e730e616712ae4caa3003e1429103fc4007e2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_0508eb5273b655eb4a4e53126c985a9b3ff3a4bf524cb5bfababfd0b76a9e0bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0508eb5273b655eb4a4e53126c985a9b3ff3a4bf524cb5bfababfd0b76a9e0bf->enter($__internal_0508eb5273b655eb4a4e53126c985a9b3ff3a4bf524cb5bfababfd0b76a9e0bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_0508eb5273b655eb4a4e53126c985a9b3ff3a4bf524cb5bfababfd0b76a9e0bf->leave($__internal_0508eb5273b655eb4a4e53126c985a9b3ff3a4bf524cb5bfababfd0b76a9e0bf_prof);

        
        $__internal_25d7bec5c2511a63f7bc552894e730e616712ae4caa3003e1429103fc4007e2a->leave($__internal_25d7bec5c2511a63f7bc552894e730e616712ae4caa3003e1429103fc4007e2a_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_b093831f855acf379247b8fedf7e6460673d58a2d2ee46c8459f93325fdb1a5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b093831f855acf379247b8fedf7e6460673d58a2d2ee46c8459f93325fdb1a5a->enter($__internal_b093831f855acf379247b8fedf7e6460673d58a2d2ee46c8459f93325fdb1a5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_e7d47174521fe3f0b063dc6b97ef4e81a6ce085c7e7e6c6e75e879b77a8d6e2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7d47174521fe3f0b063dc6b97ef4e81a6ce085c7e7e6c6e75e879b77a8d6e2a->enter($__internal_e7d47174521fe3f0b063dc6b97ef4e81a6ce085c7e7e6c6e75e879b77a8d6e2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_e7d47174521fe3f0b063dc6b97ef4e81a6ce085c7e7e6c6e75e879b77a8d6e2a->leave($__internal_e7d47174521fe3f0b063dc6b97ef4e81a6ce085c7e7e6c6e75e879b77a8d6e2a_prof);

        
        $__internal_b093831f855acf379247b8fedf7e6460673d58a2d2ee46c8459f93325fdb1a5a->leave($__internal_b093831f855acf379247b8fedf7e6460673d58a2d2ee46c8459f93325fdb1a5a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\Program Files (x86)\\EasyPHP-12.1\\www\\.git\\id2mars\\id2mars\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
